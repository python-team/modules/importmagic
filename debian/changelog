importmagic (0.1.7-7) unstable; urgency=medium

  * Team upload.
  * Redo patch to drop "six" altogether.
  * Fix SyntaxWarning (Closes: #1085645)
  * Do not generate a dependency on python3-pkg-resources (Closes: #1083437)

 -- Alexandre Detiste <tchet@debian.org>  Fri, 10 Jan 2025 16:16:33 +0100

importmagic (0.1.7-6) unstable; urgency=medium

  * Team Upload
  * Drop distutils 2/2, follow up to 0.1.7-5 (Closes: #1065878)
  * Set Rules-Requires-Root: no

 -- Alexandre Detiste <tchet@debian.org>  Wed, 19 Jun 2024 13:03:54 +0200

importmagic (0.1.7-5) unstable; urgency=medium

  * add remove-distutils.patch to support Python 3.12 (Closes: #1056412)

 -- Diane Trout <diane@ghic.org>  Mon, 04 Dec 2023 19:12:51 -0800

importmagic (0.1.7-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.1.5, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 28 May 2022 01:01:26 +0100

importmagic (0.1.7-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Sandro Tosi <morph@debian.org>  Tue, 03 May 2022 21:10:27 -0400

importmagic (0.1.7-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/watch: Use https protocol

  [ Diane Trout ]
  * Add python3-distutils as a dependency. (Closes: #896219)
  * Update debhelper version.
  * Update Standard-Version. No changes needed.

 -- Diane Trout <diane@ghic.org>  Fri, 27 Apr 2018 23:01:34 -0700

importmagic (0.1.7-1) unstable; urgency=low

  * Initial packaging (Closes: #872183)

 -- Diane Trout <diane@ghic.org>  Mon, 14 Aug 2017 21:09:33 -0700
